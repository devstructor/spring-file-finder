# README #

In order to compile the project run: 

### In order to compile the project run: ###


```
#!sh
gradle fatjar
```

### How to launch examples: ###


**Without arguments:**
```
#!sh

java -jar build/libs/spring-file-finder-1.0-SNAPSHOT.jar
```


```
Usage: DIRECTORY THREADS_COUNT [EXCLUDED_FILE_NAME]

```


**Single threaded mode:**
```
#!sh

java -jar build/libs/spring-file-finder-1.0-SNAPSHOT.jar /path/to/java/files/directory/
```


**Multiple threaded mode:**
```
#!sh

java -jar build/libs/spring-file-finder-1.0-SNAPSHOT.jar /path/to/java/files/directory/ 4
```



**Multiple threaded mode with excluding some files:**
```
#!sh

java -jar build/libs/spring-file-finder-1.0-SNAPSHOT.jar /path/to/java/files/directory/ 4 Application.java IDontWantYou.java GetDeFkkkOff.java
```



