package com.company;


import com.company.exception.ExcludedFileException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.*;
import java.util.List;
import java.util.Set;

public class SingleThreadedJavaTokenFileFinder extends TokenFileFinder {
    private static final Logger logger = LogManager.getLogger(SingleThreadedJavaTokenFileFinder.class.getName());

    public SingleThreadedJavaTokenFileFinder(List<String> tokens, String filePattern, Set<String> excludedFiles) {
        super(tokens, filePattern, excludedFiles);
    }

    @Override
    protected void doFileGrep(Path file) {
        try {
            if (excludedFiles.contains(file.toFile().getName()))
                throw new ExcludedFileException("File: "+file.getFileName()+" is excluded");

            List<String> foundTokens = fileGrep.grep(file);

            for (String token : foundTokens) {
                tokenToPathsMap.get(token).add(file);
            }
        } catch (Exception e) {
            logger.error(e);
            ++failedCount;
        }
    }
}
