package com.company;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileGrep {
    private static final Logger logger = LogManager.getLogger(FileGrep.class.getName());
    private final Map<String, Pattern> patterns;

    public FileGrep(List <String> tokens) {
        Map<String, Pattern> patternsTemp = new LinkedHashMap<>();
        for (String token : tokens) {
            if (token.startsWith("@")){
                patternsTemp.put(token, Pattern.compile("@\\b"+token.substring(1)+"\\b"));
            }
            else {
                patternsTemp.put(token, Pattern.compile("\\b"+token+"\\b"));
            }
        }
        patterns = Collections.unmodifiableMap(patternsTemp);
    }

    public List<String> grep(final Path file) throws Exception {
        final List <String> foundTokens = new ArrayList<>();
        try (InputStream in = Files.newInputStream(file);
             BufferedReader reader =  new BufferedReader(new InputStreamReader(in))) {

            String line;
            lineLoop:
            while ((line = reader.readLine()) != null) {
                if (Thread.currentThread().isInterrupted()) {
                    break;
                }

                for (Map.Entry<String, Pattern> entry : patterns.entrySet()) {
                    String token = entry.getKey();
                    Pattern pattern = entry.getValue();

                    Matcher matcher = pattern.matcher(line);
                    if (matcher.find()){
                        foundTokens.add(token);
                        break lineLoop;
                    }
                }
            }
        }
        return foundTokens;
    }
}
