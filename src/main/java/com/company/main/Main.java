package com.company.main;

import com.company.SpringFileFinder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class.getName());

    public static void main(String[] args) {
        try {
            ArgumentParser argsParser = new ArgumentParser(args);
            Path directory = argsParser.getInputDirectory();
            int threads = argsParser.getThreads();
            Set<String> excludedFiles = argsParser.getExcludedFiles();

            Runtime.getRuntime().addShutdownHook(new Thread(ShutdownHandler.getInstance()));
            List<String> tokens = Arrays.asList("@Service", "@Component");

            long start = System.currentTimeMillis();
            Map<String, List<Path>> results = findTokens(directory, threads, tokens, excludedFiles);
            long end = System.currentTimeMillis();

            printResults(results);
            logger.info("Execution of parallelizable part took: " + (end - start) + "ms");
        }
        finally {
            ShutdownHandler.getInstance().finish();
        }

    }

    private static Map<String, List<Path>> findTokens(Path directory, int threads, List<String> tokens, Set<String> excludedFiles) {
        SpringFileFinder finder = new SpringFileFinder(tokens, threads, excludedFiles);
        Map<String, List<Path>> results = finder.findInDirectory(directory);
        return results;
    }

    private static void printResults(Map<String, List<Path>> results) {
        results.forEach((name, pathList) -> {
            System.out.println(name + " : " + pathList.size() + " files");
            pathList.forEach(path -> System.out.println("\t " + path.toFile().getAbsolutePath()));
        });
    }
}
