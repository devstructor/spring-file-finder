package com.company.main;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ShutdownHandler implements Runnable{
    private final AtomicBoolean shuttingDown;
    private final CountDownLatch finish;
    private final List<ExecutorService> executorServices;

    private static class ShutdownHandlerHolder {
        private static final ShutdownHandler INSTANCE = new ShutdownHandler();
    }

    public static ShutdownHandler getInstance(){
        return ShutdownHandlerHolder.INSTANCE;
    }

    private ShutdownHandler() {
//        this.finished = new AtomicBoolean(false);
        this.executorServices = new ArrayList<>();
        this.shuttingDown = new AtomicBoolean(false);
        this.finish = new CountDownLatch(1);
    }

    @Override
    public void run() {
        shuttingDown.compareAndSet(false, true);

        executorServices.forEach(executorService -> executorService.shutdownNow());


        try {
            finish.await();
        } catch (InterruptedException swallow) {
            //swallow
        }
    }

    public void finish(){
        finish.countDown();
    }

    public boolean isShuttingDown() {
        return shuttingDown.get();
    }

    public void registerExecutorService(ExecutorService executorService){
        executorServices.add(executorService);
    }
}
