package com.company.main;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class ArgumentParser {
    private Path inputDirectory;
    private int threads = 1;
    private Set<String> excludedFiles;

    public ArgumentParser(String[] args) {
        parse(args);
    }

    private void parse(String[] args){
        if (args.length == 0){
            printHelp();
            System.exit(0);
        }

        parseInputDirectory(args);
        parseThreadsCount(args);
        parseForbiddenNames(args);
    }

    private void printHelp() {
        System.out.println("Usage: DIRECTORY THREADS_COUNT [EXCLUDED_FILE_NAME]");
    }

    private void parseInputDirectory(String[] args) {
        if (args.length < 1) {
            System.err.println("Error: Please type input directory as a first argument");
            System.exit(1);
        }
        inputDirectory = Paths.get(args[0]);
        if (!inputDirectory.toFile().isDirectory()) {
            System.err.println("Error: \"" + args[0] +"\" is not a directory");
            System.exit(1);
        }
    }

    private void parseThreadsCount(String[] args) {
        if (args.length > 1){
            String arg = args[1];
            try{
                threads = Integer.parseInt(arg);
            }
            catch (NumberFormatException e){
                System.err.println("Cannot parse threads count argument. Defaulting to " + threads);
            }
        }
    }

    private void parseForbiddenNames(String[] args) {
        excludedFiles = new HashSet<>();
        for (int i = 2; i < args.length; i++) {
            excludedFiles.add(args[i]);
        }
        excludedFiles = Collections.unmodifiableSet(excludedFiles);
    }

    public Path getInputDirectory() {
        return inputDirectory;
    }

    public int getThreads(){
        return threads;
    }

    public Set<String> getExcludedFiles() {
        return excludedFiles;
    }
}
