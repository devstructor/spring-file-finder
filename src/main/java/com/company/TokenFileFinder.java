package com.company;

import com.company.exception.ExcludedFileException;
import com.company.main.ShutdownHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

import static java.nio.file.FileVisitResult.CONTINUE;
import static java.nio.file.FileVisitResult.TERMINATE;

public abstract class TokenFileFinder {
    private static final Logger logger = LogManager.getLogger(TokenFileFinder.class.getName());
    protected int filesCount;
    protected int failedCount;
    protected final PathMatcher matcher;
    protected final List <String> tokens;
    protected final FileGrep fileGrep;
    protected Map<String, List<Path>> tokenToPathsMap;
    protected final Set<String> excludedFiles;

    public TokenFileFinder(List<String> tokens, String filePattern, Set<String> excludedFiles) {
        this.tokens = tokens;
        this.fileGrep = new FileGrep(tokens);
        if (filePattern != null)
            this.matcher = FileSystems.getDefault().getPathMatcher("glob:"+filePattern );
        else
            this.matcher = null;
        this.excludedFiles = excludedFiles;
        this.filesCount = 0;
        this.failedCount = 0;
    }

    public Map<String, List<Path>> findInDirectory(Path startingDir) {
        try {
            tokenToPathsMap = new LinkedHashMap<>();
            for (String token : tokens) {
                tokenToPathsMap.put(token, new LinkedList<>());
            }

            walkFileTree(startingDir);

            return tokenToPathsMap;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    protected void walkFileTree(Path startingDir) throws IOException {
        Files.walkFileTree(startingDir, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attr) throws IOException {
                if (ShutdownHandler.getInstance().isShuttingDown())
                    return TERMINATE;

                if (attr.isRegularFile() && (matcher == null || matcher.matches(file.getFileName())) ) {

                    doFileGrep(file);

                    if (++filesCount % 100 == 0){
                        logger.info("Visited " + filesCount + " files");
                    }
                }
                return CONTINUE;
            }
        });
    }

    protected abstract void doFileGrep(Path file);


    public int getFilesCount() {
        return filesCount;
    }

    public int getFailedCount() {
        return failedCount;
    }
}
