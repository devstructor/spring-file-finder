package com.company;

import com.company.exception.ExcludedFileException;
import com.company.main.ShutdownHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.*;

public class MultiThreadedJavaTokenFileFinder extends TokenFileFinder {
    private static final Logger logger = LogManager.getLogger(MultiThreadedJavaTokenFileFinder.class.getName());

    private final int threads;
    private ExecutorService executorService;
    private Map<Path, Future<List<String>>> futures;

    public MultiThreadedJavaTokenFileFinder(List<String> tokens, String filePattern, Set<String> excludedFiles, int threads) {
        super(tokens, filePattern, excludedFiles);
        this.threads = threads;
    }


    @Override
    protected void doFileGrep(Path file) {
        if (!executorService.isShutdown()){
            Future<List<String>> future = executorService.submit(new Callable<List<String>>() {
                @Override
                public List<String> call() throws Exception {
                    if (excludedFiles.contains(file.toFile().getName()))
                        throw new ExcludedFileException("File: "+file.getFileName()+" is excluded");

                    List<String> result = fileGrep.grep(file);
                    return result;
                }
            });
            futures.put(file, future);
        }
    }


    @Override
    protected void walkFileTree(Path startingDir) throws IOException {
        futures = new HashMap<>();
        executorService = Executors.newFixedThreadPool(threads);
        ShutdownHandler.getInstance().registerExecutorService(executorService);

        super.walkFileTree(startingDir);

        executorService.shutdown();
        try {
            executorService.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException swallow) {
            //swallow
        }
        collectResults();
    }

    private void collectResults() {
        for (Map.Entry<Path, Future<List<String>>> entry : futures.entrySet()) {
            Path file = entry.getKey();
            Future<List<String>> future = entry.getValue();

            try {
                if (future.isDone()){
                    List<String> tokens = future.get();
                    for (String token : tokens) {
                        tokenToPathsMap.get(token).add(file);
                    }
                }
            } catch (InterruptedException e) {
                logger.error(e.getMessage(), e);
            } catch (ExecutionException e) {
                ++failedCount;
                logger.error(e);
            }
        }
    }


}
