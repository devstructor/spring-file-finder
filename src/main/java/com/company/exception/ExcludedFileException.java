package com.company.exception;


public class ExcludedFileException extends Exception {

    public ExcludedFileException(String message) {
        super(message);
    }

    public ExcludedFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExcludedFileException(Throwable cause) {
        super(cause);
    }
}
