package com.company;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class SpringFileFinder {
    private static final Logger logger = LogManager.getLogger(SpringFileFinder.class.getName());
    public static final String JAVA_FILE_PATTERN = "*.java";

    TokenFileFinder tokenFileFinder;

    public SpringFileFinder(List<String> tokens, int threads, Set<String> excludedFiles) {
        if (threads == 1){
            tokenFileFinder = new SingleThreadedJavaTokenFileFinder(tokens, JAVA_FILE_PATTERN, excludedFiles);
        }
        else if (threads > 1){
            tokenFileFinder = new MultiThreadedQueuedJavaTokenFileFinder(tokens, JAVA_FILE_PATTERN, excludedFiles, threads);
        }
        else{
            throw new IllegalStateException("Thread count cannot be negative! Given: " + threads);
        }
    }
    public Map<String, List<Path>> findInDirectory(Path startingDir) {
        Map<String, List<Path>> result = tokenFileFinder.findInDirectory(startingDir);
        logger.info("Total number of analysed files: " + tokenFileFinder.getFilesCount());
        logger.info("Number of files failed to analyse: " + tokenFileFinder.getFailedCount());
        return result;
    }


}
