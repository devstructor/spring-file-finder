package com.company;

import com.company.exception.ExcludedFileException;
import com.company.main.ShutdownHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class MultiThreadedQueuedJavaTokenFileFinder extends TokenFileFinder {
    private static final Logger logger = LogManager.getLogger(MultiThreadedQueuedJavaTokenFileFinder.class.getName());

    private final int threads;
    private BlockingQueue<Path> queue;
    private ExecutorService executorService;
    private List <Exception> exceptions;
    private AtomicInteger activeProducers = new AtomicInteger(0);

    public MultiThreadedQueuedJavaTokenFileFinder(List<String> tokens, String filePattern, Set<String> excludedFiles, int threads) {
        super(tokens, filePattern, excludedFiles);
        this.threads = threads;
        this.queue = new LinkedBlockingDeque<>();
        this.exceptions = new ArrayList<>();
    }

    @Override
    protected void doFileGrep(Path file) {
        try {
            queue.put(file);
        } catch (InterruptedException swallow) {
            //swallow
        }
    }

    @Override
    protected void walkFileTree(Path startingDir) throws IOException {
        executorService = Executors.newFixedThreadPool(threads);
        ShutdownHandler.getInstance().registerExecutorService(executorService);

        spawnConsumers();

        activeProducers.incrementAndGet();
        super.walkFileTree(startingDir);
        activeProducers.decrementAndGet();

        executorService.shutdown();
        try {
            executorService.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            //swallow
        }


        failedCount += exceptions.size();
        exceptions.forEach(logger::error);
    }

    private void spawnConsumers() {
        for (int i = 0; i < threads; i++) {
            executorService.submit(() -> {
                while (activeProducers.get() > 0 || !queue.isEmpty()){
                    if (Thread.currentThread().isInterrupted())
                        break;

                    Path file = queue.poll();
                    if (file != null){
                        try {
                            if (excludedFiles.contains(file.toFile().getName())) {
                                throw new ExcludedFileException("File: " + file.getFileName() + " is excluded");
                            }

                            List<String> tokens1 = fileGrep.grep(file);
                            for (String token : tokens1) {
                                synchronized (token) {
                                    tokenToPathsMap.get(token).add(file);
                                }
                            }
                        }
                        catch (Exception e){
                            synchronized (exceptions){
                                exceptions.add(e);
                            }
                        }
                    }

                }
            });
        }
    }
}
